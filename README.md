# Power meter for the gnome panel

Displays the current power in watts +to or -from the laptop battery. If the charger is connected and battery is fully charged, it displays 0W.

## Requires

sudo apt install gir1.2-appindicator3-0.1

## Run

git clone this repo, cd into it and run ./power.py. The applet should appear in the upper right of you screen.
